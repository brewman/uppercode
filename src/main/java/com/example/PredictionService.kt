package com.example

import com.example.entity.Card
import com.example.entity.Category
import org.springframework.stereotype.Service

@Service
class PredictionService(private val categoryRepository: CategoryRepository, val cardRepository: CardRepository) {

    fun suggestSeated(cards: List<Pair<Card, Boolean>>): Set<Card> {
        return suggest(cards.filter { it.first.dataSet == "POI" }, categoryRepository.poiMap,cardRepository.poiMap).union(
                suggest(cards.filter { it.first.dataSet == "Gastro" }, categoryRepository.gastroMap,cardRepository.gastroMap)).union(
                suggest(cards.filter { it.first.dataSet == "Tour" }, categoryRepository.tourMap,cardRepository.tourMap)).union(
                suggest(cards.filter { it.first.dataSet == "Event" }, categoryRepository.eventMap,cardRepository.eventMap)).union(
                suggest(cards.filter { it.first.dataSet == "Hotel" }, categoryRepository.hotelMap, cardRepository.hotelMap))

    }

    private fun suggest(cards: List<Pair<Card, Boolean>>, map: Map<Int, Category>, cardMap: Map<Int, Card>): List<Card> {
        if(cards.isEmpty())return emptyList()

        val categoryAmount = map.size
        val generalVector = IntArray(categoryAmount) { i -> 0 }

        for (cardId in cards) {
            val card = cardId.first
            map.values.forEachIndexed { index, x ->
                run {
                    if (card.categories.contains(x.name)) {
                        if (cardId.second)
                            generalVector[index] += 1
                        else
                            generalVector[index] -= 1
                    }
                }
            }
        }
        val normalisedVector = normaliseToHalf(generalVector, cards.size)

        var smallest: Card = cardMap[0]!!
        var distance = 100000000000000.0f

        for (card in cardMap.values) {
            val vector = IntArray(categoryAmount) { _ -> 0 }

            map.values.forEachIndexed { index, x ->
                run {
                    if (card.categories.contains(x.name)) {
                        vector[index] += 1
                    }
                }
            }
            if (distance(normalisedVector, vector) < distance) {
                smallest = card
                distance = distance(normalisedVector, vector)
            }
        }

        return listOf(smallest)
    }

    fun normaliseToHalf(array: IntArray, amount: Int): DoubleArray {
        return array.map { i -> ((i.toFloat() / amount) / 2) + 0.5 }.toDoubleArray()
    }

    fun normalise(array: IntArray): FloatArray {
        val max: Int = array.max()!!
        return array.map { i -> i.toFloat() / max }.toFloatArray()
    }

    fun sigmod(array: IntArray): DoubleArray {
        return array.map { i -> 1.0 / (1 + Math.pow(Math.E, -i.toDouble())) }.toDoubleArray()
    }

    fun distance(array: DoubleArray, array2: IntArray): Float {
        return array.mapIndexed { index, fl -> Math.pow(fl - array2[index].toDouble(), 2.0).toFloat() }.sum()
    }

}
