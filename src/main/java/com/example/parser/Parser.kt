package com.example.parser

import com.example.entity.Card
import com.example.entity.Category
import java.io.File
import javax.xml.parsers.DocumentBuilderFactory

fun main(args: Array<String>) {
    parseCategories("src/main/resources/Gastro_Kategorien.xml")
}

fun parseCategories(path: String): List<Category> {
    val inputFile = File(path)
    val dbFactory = DocumentBuilderFactory.newInstance()
    val dBuilder = dbFactory.newDocumentBuilder()
    val doc = dBuilder.parse(inputFile)
    doc.documentElement.normalize()

    val categories = ArrayList<Category>()
    val categoryNode = doc.documentElement.getElementsByTagName("Category")

    var lastLevel = ""
    for (i in 0 until categoryNode.length) {
        val category = categoryNode.item(i)

        if (category.attributes.getNamedItem("Level") != null) {
            if (!categories.isEmpty() && categories.last().categories.isEmpty() &&
                    category.attributes.getNamedItem("Level").textContent != lastLevel)
                categories.remove(categories.last())

            lastLevel = category.attributes.getNamedItem("Level").textContent
            categories.add(Category(category.attributes.getNamedItem("Name").textContent, HashSet()))
        } else {
            lastLevel = ""
            categories.last().categories.add(category.attributes.getNamedItem("Name").textContent)
        }
    }
    return categories
}

fun parse(path: String, dataSet: String): List<Card> {
    val inputFile = File(path)
    val dbFactory = DocumentBuilderFactory.newInstance()
    val dBuilder = dbFactory.newDocumentBuilder()
    val doc = dBuilder.parse(inputFile)
    doc.documentElement.normalize()

    val cards = ArrayList<Card>()
    val items = doc.documentElement.getElementsByTagName("item")
    for (i in 0 until items.length) {
        val nNode = items.item(i)

        val childs = nNode.childNodes

        var guid = ""
        var link = ""
        var title = ""
        var latitude = 0.0
        var longitude = 0.0
        var city = ""
        var description = ""
        val categories = HashSet<String>()
        val images = HashSet<String>()
        for (j in 0 until childs.length) {
            if (childs.item(j).nodeName == "guid")
                guid = childs.item(j).textContent
            else if (childs.item(j).nodeName == "ec:link" && childs.item(j).attributes.getNamedItem("rel").textContent == "other")
                link = childs.item(j).attributes.getNamedItem("url").textContent
            else if (childs.item(j).nodeName == "ec:link" && (childs.item(j).attributes.getNamedItem("rel").textContent == "summer"
                            || childs.item(j).attributes.getNamedItem("rel").textContent == "winter"
                            || childs.item(j).attributes.getNamedItem("rel").textContent == "default"))
                images.add(childs.item(j).attributes.getNamedItem("url").textContent)
            if (childs.item(j).nodeName == "title")
                title = childs.item(j).textContent
            if (childs.item(j).nodeName == "category")
                categories.add(childs.item(j).textContent)
            if (childs.item(j).nodeName == "geo:lat")
                latitude = java.lang.Double.parseDouble(childs.item(j).textContent)
            if (childs.item(j).nodeName == "geo:long")
                longitude = java.lang.Double.parseDouble(childs.item(j).textContent)
            if (childs.item(j).nodeName == "ec:city")
                city = childs.item(j).textContent
            if (childs.item(j).nodeName == "shortdescription") {
                description = "\\<.*?>".toRegex().replace(childs.item(j).textContent, "")
            }
        }

        cards.add(Card(guid, link, images, title, categories, latitude, longitude, city, dataSet, description))
    }
    return cards
}