package com.example.dto

data class CardResponseDTO(var guid: String, var direction:String)

data class CardResponseContainerDTO(var cards: List<CardResponseDTO>, var userId: String?)