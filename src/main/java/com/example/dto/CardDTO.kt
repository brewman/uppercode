package com.example.dto

import com.example.entity.Card

data class CardContainerDTO(var cards: List<CardDTO>, var endScreen: Boolean, var userId: String)

data class CardDTO(var guid: String, var link: String, var images: Set<String>, var title:String, var dataset: String, var city: String, var description: String)

fun Card.toDTO() = CardDTO(
        guid = guid,
        link = link,
        images = images,
        title = title,
        dataset = dataSet,
        city = city,
        description = description)