package com.example.entity

data class Category(var name:String, var categories: HashSet<String>)