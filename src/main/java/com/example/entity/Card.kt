package com.example.entity

data class Card(
        var guid: String,
        var link: String,
        var images: Set<String>,
        var title:String,
        var categories: Set<String>,
        var latitude: Double,
        var longitude: Double,
        var city: String,
        var dataSet: String,
        var description: String)