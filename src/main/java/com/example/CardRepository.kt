package com.example

import com.example.entity.Card
import com.example.parser.parse
import org.springframework.stereotype.Service
import java.util.*

@Service
class CardRepository {
    val poiMap: HashMap<Int, Card> = HashMap()
    val eventMap: HashMap<Int, Card> = HashMap()
    val gastroMap: HashMap<Int, Card> = HashMap()
    val hotelMap: HashMap<Int, Card> = HashMap()
    val tourMap: HashMap<Int, Card> = HashMap()


    init {
        for ((i, x) in parse("src/main/resources/POI_de_1000.xml", "POI").withIndex()) {
            poiMap[i] = x
        }
        for ((i, x) in parse("src/main/resources/Event_de_1000.xml", "Event").withIndex()) {
            eventMap[i] = x
        }
        for ((i, x) in parse("src/main/resources/Gastro_de_1000.xml", "Gastro").withIndex()) {
            gastroMap[i] = x
        }
        for ((i, x) in parse("src/main/resources/Tour_de_1000.xml", "Tour").withIndex()) {
            hotelMap[i] = x
        }
        for ((i, x) in parse("src/main/resources/Hotel_de_1000.xml", "Hotel").withIndex()) {
            tourMap[i] = x
        }
    }

    fun getRandomCards(amount: Int): List<Card> {
        val dummyList: ArrayList<Card> = ArrayList()

        for (x in 0..amount) {
            dummyList.add(poiMap[(Math.random() * poiMap.size).toInt()] ?: continue)
            dummyList.add(hotelMap[(Math.random() * hotelMap.size).toInt()] ?: continue)
            dummyList.add(gastroMap[(Math.random() * gastroMap.size).toInt()] ?: continue)
            dummyList.add(eventMap[(Math.random() * eventMap.size).toInt()] ?: continue)
            dummyList.add(tourMap[(Math.random() * tourMap.size).toInt()] ?: continue)
        }
        dummyList.shuffle()
        return dummyList
    }

    fun find(guid: String): List<Card> {
        return poiMap.values.union(hotelMap.values).union(tourMap.values).union(gastroMap.values).union(eventMap.values)
                .filter { it.guid == guid }
    }
}