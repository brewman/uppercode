package com.example.controller

import com.example.CardRepository
import com.example.PredictionService
import com.example.UserRepository
import com.example.dto.*
import com.example.entity.Card
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class DefaultController(private val cardRepository: CardRepository, private val predictionService: PredictionService, private val userRepository: UserRepository) {
    @CrossOrigin
    @PostMapping("api/v1/cards/{amount}")
    fun getCards(@PathVariable amount: Int, @RequestBody responseBody: CardResponseContainerDTO): CardContainerDTO {
        val userId= responseBody.userId?: UUID.randomUUID().toString()

        responseBody.cards.map { Pair(it.guid, it.direction) }.forEach { userRepository.addResponse(it, userId) }

        if (userRepository.countParis(userId) > 25) {
            return CardContainerDTO(predictionService.suggestSeated(userRepository.getPairs(userId)).map { it.toDTO() }, true, userId)
        }

        return CardContainerDTO(cardRepository.getRandomCards(amount - 1).map { it.toDTO() }, false, userId)
    }

    @CrossOrigin
    @GetMapping("api/v1/questions/{amount}")
    fun getQuestions(@PathVariable amount: Int): List<Card> {
        return cardRepository.getRandomCards(amount)
    }
}