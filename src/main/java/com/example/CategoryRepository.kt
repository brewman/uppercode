package com.example

import com.example.entity.Card
import com.example.entity.Category
import com.example.parser.parseCategories
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList

@Service
class CategoryRepository
{
    val poiMap: HashMap<Int, Category> = HashMap()
    val eventMap: HashMap<Int, Category> = HashMap()
    val gastroMap: HashMap<Int, Category> = HashMap()
    val hotelMap: HashMap<Int, Category> = HashMap()
    val tourMap: HashMap<Int, Category> = HashMap()

    init {
        for ((i, x) in parseCategories("src/main/resources/POI_Kategorien.xml").withIndex()) {
            poiMap[i] = x
        }
        for ((i, x) in parseCategories("src/main/resources/Event_Kategorien.xml").withIndex()) {
            eventMap[i] = x
        }
        for ((i, x) in parseCategories("src/main/resources/Gastro_Kategorien.xml").withIndex()) {
            gastroMap[i] = x
        }
        for ((i, x) in parseCategories("src/main/resources/Hotel_Kategorien.xml").withIndex()) {
            hotelMap[i] = x
        }
        for ((i, x) in parseCategories("src/main/resources/Tour_Kategorien.xml").withIndex()) {
            tourMap[i] = x
        }
    }
}

@Service
class UserRepository(private val cardRepository: CardRepository)
{
    private val userMap: HashMap<String, ArrayList<Pair<Card,Boolean>>> = HashMap()

    fun addResponse(pair: Pair<String,String>, userId:String)
    {
        if(userMap[userId]==null)
        {
            userMap[userId]= ArrayList()
        }
        else {
            val direction= pair.second=="right"
            val card = cardRepository.find(pair.first).firstOrNull()?: return
            userMap[userId]!!.add(Pair(card,direction))
        }
    }

    fun getPairs(userId:String) : List<Pair<Card,Boolean>>
    {
        return userMap[userId]?: emptyList()
    }

    fun countParis(userId: String):Int = userMap[userId]?.size ?:0

}