var prefab;
let resultArray = {cards:[]};
var userId;
var mock = false;
$(document).ready(function () {

  
  var animating = false;
  var cardsCounter = 0;
  var numOfCards = 6;
  var decisionVal = 80;
  var pullDeltaX = 0;
  var deg = 0;
  var $card, $cardReject, $cardLike;

  function pullChange() {
    animating = true;
    deg = pullDeltaX / 10;
    /*if (deg > -5 && deg < 5)
      return;*/
    $card.css(
      'transform',
      'translateX(' + pullDeltaX + 'px) rotate(' + deg + 'deg)'
    );

    var opacity = pullDeltaX / 100;
    var rejectOpacity = opacity >= 0 ? 0 : Math.abs(opacity);
    var likeOpacity = opacity <= 0 ? 0 : opacity;
    $cardReject.css('opacity', rejectOpacity);
    $cardLike.css('opacity', likeOpacity);


  }
  var cards = [];

  function getNewCards(func) {
    //var url = 'https://localhost:5000/api/v1/cards/4';
    var url = '/api/v1/cards/1';

    let blah = resultArray;
$.ajax({
  url: url,
  cache: false,
  method: 'POST',
  data: JSON.stringify(resultArray),
  dataType: 'json',
  contentType: 'application/json; charset=utf-8',
  success: function(result) {
    resultArray = {};
    resultArray.cards = [];

    if (result.endScreen) {
      showEndScreen(result);
    } else {
      /*if (mock) {
        response = {
          cards: [
            {
              guid: '123',
              img:
                'https://media.tourdata.at/file/original/bcb454633f245dbec9ce798c33111b6f.jpg',
              link:
                'https://www.oberoesterreich.at/oesterreich-poi/detail/48200088/coina-cosmetic-institut-birgit-appesbacher.html',
              title: 'COINA - Cosmetic Institut Birgit Appesbacher'
            },
            {
              guid: '456',
              img:
                'https://media.tourdata.at/file/original/43219ca805096670ec8e8fb4e87d720e.jpg',
              link: 'http://www.wollgartl.at/',
              title: 'Wollgartl'
            }
          ]
        };
      }*/
      cardsDownloaded(result);
    }
  }
});

    //return response.cards;
  }

  
  function release() {
    var dir;
    if (pullDeltaX >= decisionVal) {
      dir = "right";
      $card.addClass('to-right');
    } else if (pullDeltaX <= -decisionVal) {
      dir = "left";
      $card.addClass('to-left');
    }

    if (Math.abs(pullDeltaX) >= decisionVal) {
      $card.addClass('inactive');

      setTimeout(function () {
        $card.remove();
        var cardGuid = $card.find('.demo__card__guid').text();
        resultArray.userId = userId;
        resultArray.cards.push({ guid: cardGuid, direction: dir });
        var container = $('.demo__card-cont');
        if (container.children().length < 5) {
          getNewCards();
        }

        return;
        $card.addClass('below').removeClass('inactive to-left to-right');
        cardsCounter++;
        if (cardsCounter === numOfCards) {
          cardsCounter = 0;
          $('.demo__card').removeClass('below');
        }
      }, 300);
    }

    if (Math.abs(pullDeltaX) < decisionVal) {
      $card.addClass('reset');
    }

    setTimeout(function() {
      $card
        .attr('style', 'visibility: visible;')
        .removeClass('reset')
        .find('.demo__card__choice')
        .attr('style', '');

      pullDeltaX = 0;
      animating = false;
    }, 300);
  }

  $(document).on('mousedown touchstart', '.demo__card:not(.inactive)', function(
    e
  ) {
    if (animating) return;

    $card = $(this);
    $cardReject = $('.demo__card__choice.m--reject', $card);
    $cardLike = $('.demo__card__choice.m--like', $card);
    var startX = e.pageX || e.originalEvent.touches[0].pageX;

    $(document).on('mousemove touchmove', function(e) {
      var x = e.pageX || e.originalEvent.touches[0].pageX;
      pullDeltaX = x - startX;
      if (!pullDeltaX) return;
      pullChange();
    });

    $(document).on('mouseup touchend', function() {
      $(document).off('mousemove touchmove mouseup touchend');
      if (!pullDeltaX) return; // prevents from rapid click events
      release();
    });
  });
  
    prefab = $('.demo__card');

  cards = getNewCards();
});

function addCards(cards) {
  var container = $('.demo__card-cont');

  for (var i = 0; i < cards.length; ++i) {
    var currentCard = cards[i];

    var newCard = prefab.clone();
    var bg = newCard.find('.demo__card__img');
    bg.css('background-image', 'url("' + currentCard.images[0] + '")');
    bg.css('background-position', 'center');
    newCard.css('visibility', 'visible');
    var link = newCard.find('.demo__card__link');
    link.text(currentCard.link);
    var guid = newCard.find('.demo__card__guid');
    guid.text(currentCard.guid);
    var textElement = newCard.find('.demo__card__we');
    textElement.text(currentCard.title);
    var cityElement = newCard.find('.demo__card__loc');
    cityElement.text(currentCard.city);
    var descrElement = newCard.find('.demo__card__descr');
    var maxLength = 150;
    if (currentCard.description.length > maxLength) {
      currentCard.description =
        currentCard.description.substring(0, maxLength-3) + '...';
    }
    descrElement.text(currentCard.description);

      


    var iconElement = newCard.find('.icon__POI');
    iconElement.removeClass('icon__POI');
    iconElement.addClass('icon__' + currentCard.dataset);

    container.prepend(newCard);
  }
}

function showEndScreen(result) {
  var demoElem = $(document)
    .find('.demo__content');
  demoElem.hide();


  $endscreenElement = $(document).find('.endscreen__content');

  $endscreenElement.show();
  var totalPrice = 0;
  for (var i = 0; i < result.cards.length; ++i) {
    var currentCard = result.cards[i];
    var price = Math.floor(Math.random() * 101) + 60;
    totalPrice += price;
    // fill endscreen with result
    $element = $endscreenElement.find('.endscreen_' + currentCard.dataset);
    $element.find('.endscreen__card__imagetext').text(currentCard.title);
    $element
      .find('.endscreen__card__imageimage')
      .css('background-image', 'url(' + currentCard.images[0] + ')');
    
    $element.find('.endscreen__card__price').text('€ ' + price + '.99');

  }

  $elem = $(document)
    .find('.endscreen__footer');

    $elem.html(
      '<img src="images/Geldboerse.png" style="width:55px; margin-right: 10px;"><b>' +
        '€ ' +
        totalPrice +
        '</b>'
    );


}

var num = 1;
var descriptions = [
  'Hast du heute schon Leitungswasser getrunken?',
  'Hast du heute schon statt dem Auto die Öffis benutzt?',
  'Hast du heute schon deinen Plastikmüll reduziert?',
  'Hast du heute schon beim Einkauf auf Regionalität geachtet?',
  'Hast du heute schon auf deinen Wasserverbrauch geachtet?'
];
function cardsDownloaded(result) {
  userId = result.userId;
  result.cards.push({
    title: descriptions[num-1],
    images: [
      'images/q' + num + '.png'
    ],
    dataset: "Question",
    description: "Dein Beitrag zur Nachhaltigkeit.",
    city: "Egal wo du bist."
  });
  num++;
  addCards(result.cards);

  $('.demo__card__btm').click(function(e) {
    $card = $(this).parent();
    $cardLink = $card.find('.demo__card__link');
    var link = $cardLink.text();
    ConfirmDialog('Gehe zu', link);
  });

  $('.demo__card__top').click(function(e) {
    $el = $(this);
    var posX = $(this).position().left,
      posY = $(this).position().top;
    alert(e.pageX - posX + ' , ' + (e.pageY - posY));
  });
}

function ConfirmDialog(message, link) {
  $('<div></div>')
    .appendTo('body')
    .html('<div><h6>' + message + '?</h6></div>')
    .dialog({
      modal: true,
      title: 'Delete message',
      zIndex: 10000,
      autoOpen: true,
      width: 'auto',
      resizable: false,
      buttons: {
        Yes: function() {
          // $(obj).removeAttr('onclick');
          // $(obj).parents('.Parent').remove();

          $(this).dialog('close');

          window.location.replace(link);
        },
        No: function() {
          $(this).dialog('close');
        }
      },
      close: function(event, ui) {
        $(this).remove();
      }
    });
}
